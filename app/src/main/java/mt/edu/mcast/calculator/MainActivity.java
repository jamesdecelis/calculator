package mt.edu.mcast.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView display;
    int num1, num2;
    char operator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display = findViewById(R.id.txtvDisplay);
    }

    public void numbersClicked (View v){
        Button b = (Button)v;

        display.setText(display.getText() + b.getText().toString());
    }

    public void operatorClicked(View v){

        if(display.getText().length() > 0) {
            num1 = Integer.parseInt(display.getText().toString());

            switch (v.getId()) {

                case R.id.btnPlus:
                    operator = '+';
                    break;
                case R.id.btnMinus:
                    operator = '-';
                    break;
                case R.id.btnMult:
                    operator = '*';
                    break;
                case R.id.btnDiv:
                    operator = '/';
                    break;
            }//switch

            display.setText("");
        }

    }

    public void working(View v){
        if(display.getText().length() > 0) {
            num2 = Integer.parseInt(display.getText().toString());

            int ans = 0;

            switch (operator) {

                case '+':
                    ans = num1 + num2;
                    break;
                case '-':
                    ans = num1 - num2;
                    break;
                case '*':
                    ans = num1 * num2;
                    break;
                case '/':
                    ans = num1 / num2;
                    break;

            }//switch

            display.setText(String.valueOf(ans));
        }
    }
}
